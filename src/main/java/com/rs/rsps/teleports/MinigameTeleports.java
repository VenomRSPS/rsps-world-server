package com.rs.rsps.teleports;

import com.rs.lib.game.Tile;

public class MinigameTeleports {

	public static Teleport[] VALUES = {
			new Teleport("Clan Wars", Tile.of(2994, 9679, 0)),
			new Teleport("Stealing Creation", Tile.of(2994, 9679, 0)),
			//new Teleport("Soul Wars", Tile.of(2994, 9679, 0)),
			//new Teleport("Fist of Guthix", Tile.of(2994, 9679, 0)),
			new Teleport("Castle Wars", Tile.of(2994, 9679, 0)),
			//new Teleport("Barbarian Assault", Tile.of(2994, 9679, 0)),
			new Teleport("Pest Control", Tile.of(2994, 9679, 0)),
			new Teleport("Fight Caves", Tile.of(2994, 9679, 0)),
			new Teleport("Fight Pits", Tile.of(2994, 9679, 0)),
			new Teleport("Fight Kiln", Tile.of(2994, 9679, 0)),
			new Teleport("Brimhaven Agility Arena", Tile.of(2994, 9679, 0)),
			new Teleport("Fishing Trawler", Tile.of(2994, 9679, 0)),
			//new Teleport("Flash Powder Factory", Tile.of(2994, 9679, 0)),
			//new Teleport("Great Orb Project", Tile.of(2994, 9679, 0)),
			new Teleport("Impetuous Impulses", Tile.of(2994, 9679, 0)),
			new Teleport("Pyramid Plunder", Tile.of(2994, 9679, 0)),
			new Teleport("Sorceress's Garden", Tile.of(2994, 9679, 0)),
			//new Teleport("Trouble Brewing", Tile.of(2994, 9679, 0)),
			//new Teleport("Vinesweeper", Tile.of(2994, 9679, 0)),
			new Teleport("Shades of Mort'ton", Tile.of(2994, 9679, 0)),
			//new Teleport("Tai Bwo Wannai Cleanup", Tile.of(2994, 9679, 0))
	};
	
}
